﻿# Caoutchouc
This library allows the user to parse strings using an "interaction tree". For instance it can be used to create tchat AI.
This library is under active development and is only at its begining.


<a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Caoutchouc</span> de <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Caoutchouc</span> est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/">licence Creative Commons Attribution - Pas de Modification 4.0 International</a>.
<b>This is not the final liscense. This library will be open to modifications in a near future.</b>
